from collections import namedtuple
from operator import add, mul

import enum
import numba
import numpy as np
from numba import float32, float64
from six.moves import reduce, range

__all__ = ['custom_threshold', 'prod', 'concat', 'MbitMask', 'reduce', 'range', 'DetCtx','IsGoodFlag', 'X340EVR', 'roi_mask']

@numba.vectorize([
    float32(float32, float32, float32, float32, float32),
    float64(float64, float64, float64, float64, float64),
], nopython=True)
def _threshold(data, lower, lvalue, upper, uvalue):
    if data >= lower and data < upper:
        return data

    if data > upper: 
        return uvalue
    else: 
        return lvalue

def roi_mask(x, y, r):
    x = np.arange(768) - x
    y = np.arange(704) - y
    X, Y = np.meshgrid(x, y)
    R = np.sqrt(X**2 + Y**2)
    theta = np.arctan2(X, Y)
    return R < r
    
def custom_threshold(data, lower=None, lvalue=None, upper=None, uvalue=None):
    lower = -np.inf if lower is None else lower
    upper = np.inf if upper is None else upper
    
    lvalue = lower if lvalue is None else lvalue
    uvalue = upper if uvalue is None else uvalue
    
    return _threshold(data, lower, lvalue, upper, uvalue)

def prod(seq):
    return reduce(mul, seq)

def concat(iter):
    return reduce(add, iter)

class X340EVR(enum.IntEnum):
    NONE = 0
    LASERON = 1<<0
    LASEROFF = 1<<1
    XFELOFF = 1<<2
    MISC = 1<<3
    MISSING = 1<<4

class IsGoodFlag(enum.IntEnum):
    NONE = 0
    GOOD = 1

class MbitMask(enum.IntEnum):
    NONE = 0             # - returns None
    PIXEL_STATUS = 1     # - pixel_status ("bad pixels" deployed by calibman)
    PIXEL_MASK = 2       # - pixel_mask (deployed by user in "pixel_mask" calib dir)
    EDGE_PIXELS = 4      # - edge pixels
    CSPAD_CENTRAL = 8    # - big "central" pixels of a cspad2x1
    UNBONDED = 16        # - unbonded pixels
    UNBONDED4 = 32       # - unbonded pixel with four neighbors
    UNBONDED8 = 64       # - unbonded pixel with eight neighbors
    ALL = NONE | PIXEL_STATUS | PIXEL_MASK | EDGE_PIXELS | CSPAD_CENTRAL | UNBONDED | UNBONDED4 | UNBONDED8

DetCtx = namedtuple('DetCtx', ['callable', 'dtype', 'mpi_dtype', 'data'])
