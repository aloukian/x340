import click
import RegDB.experiment_info as experiment_info
import shlex
import subprocess
import os
import time

submit_template = "bsub -q psnehhiprioq -o logs/run-{run}.log -n 32 mpirun --mca btl ^openib python parallel-reduce.py {run}"
hutch = "xpp"
experiment_name = "xpplu4817"

def submit_job(run):
    print("Submitting run {run}".format(run=run))
    args = submit_template.format(run=run)
    args = shlex.split(args)
    print(subprocess.check_output(args))


def check_files_copied(run):
    if experiment_info is None:
        raise RuntimeError('cannot access experiment info')

    expid = experiment_info.name2id(experiment_name)
    offline_path = "/reg/d/psdm/{hutch}/{experiment_name}/xtc/{filename}"
    fileinfo = experiment_info.get_open_files(expid, run)

    for f in fileinfo:
        p = os.path.split(f['dirpath'])[-1]
        outpath = offline_path.format(filename=p,
            experiment_name=experiment_name, hutch=hutch)
        #print('searching for file {}'.format(outpath))

        if not os.path.exists(outpath):
            #print('file not found {}'.format(outpath))
            return False

    #print('all files present')
    return True

@click.command()
@click.argument("minrun", default=-1)
def monitor(minrun):
    """autosubmit parallel reduce jobs when all files have been copied,
    starting from number minrun
    """
    completed_runs = experiment_info.experiment_runs(hutch.upper(), experiment_name)
    minrun = completed_runs[-1]['num'] if minrun == -1 else minrun

    # fill set with runs smaller than minrun
    processed = list(filter(lambda x: int(x['num']) < minrun, completed_runs))
    processed = set([int(x['num']) for x in processed])
    
    while True:
        # collect candidates
        completed_runs = experiment_info.experiment_runs(hutch.upper(), experiment_name)
        updates = filter(lambda x: int(x['num']) not in processed, completed_runs)
        updates = [int(x['num']) for x in updates]

        # submit jobs
        for run in updates:
            print('Found new job, run {}'.format(run))
            if check_files_copied(run):
              submit_job(run)
              processed.add(run)

        time.sleep(5.0)

if __name__ == '__main__':
    monitor()
