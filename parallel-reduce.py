import time

import h5py
import numpy as np
import psana
import toolz
import click
import os.path
from skimage import draw
from pyimgalgos.HPolar import cart2polar

from mpi4py import MPI

from x340helpers import *

exp_name = "xpplu4817"
datasource_template = "exp={exp_name}:run={run}:idx"
output_dir = "/reg/d/psdm/xpp/xpplu4817/results/hdf5_anton"
#output_dir = "/reg/d/psdm/xpp/xpplu4817/scratch/aloukian/test"
output_template = "{exp_name}-run{run}-reduced.h5"

TIMESTAMP_DTYPE = MPI.INT64_T.Create_contiguous(2)
TIMESTAMP_DTYPE.Commit()

EPICS_DTYPE = MPI.DOUBLE.Create_contiguous(16)
EPICS_DTYPE.Commit()

ARRAYDET_DTYPE = MPI.Datatype.Create_struct([1, 1], [0, 8], [MPI.DOUBLE, MPI.INT64_T])
ARRAYDET_DTYPE.Commit()

EPIXDET_DTYPE = MPI.DOUBLE.Create_contiguous(2)
EPIXDET_DTYPE.Commit()

IPMDET_DTYPE = MPI.DOUBLE.Create_contiguous(2)
IPMDET_DTYPE.Commit()

CONTROL_DTYPE = MPI.Datatype.Create_struct([1,], [0,], [MPI.DOUBLE,])
CONTROL_DTYPE.Commit()

timestamp_dtype = np.dtype([('ts', np.int64), ('fid', np.int64)])

comm = MPI.COMM_WORLD
size = comm.Get_size()
rank = comm.Get_rank()

def configure_detectors(detnames, run):
    detectors = {name: psana.Detector(name) for name in detnames}
    
    # construct epix circular mask
    #epix_mask = np.zeros(epix_shape, dtype=np.float32)
    #epix_center = [355.3, 383.9]
    #epix_radius = 200
    #cindx = draw.circle(epix_center[0], epix_center[1], epix_radius,
    #    shape=epix_shape)
    #epix_mask[cindx] = 1.

    # construct epix ROIs
    epix_shape = detectors['epix_1'].shape()
    epix_mask1 = np.zeros(epix_shape, dtype=np.float32)
    epix_mask2 = np.zeros(epix_shape, dtype=np.float32)
    y, x = np.array([150, 150, 550, 550]), np.array([40, 240, 240, 40]) 
    cindx1 = draw.polygon(y, x, shape=epix_shape)
    cindx2 = draw.polygon(y, x+320, shape=epix_shape)

    epix_mask1[cindx1] = 1.
    epix_mask2[cindx2] = 1.

    # construct cspad radial mask
    cspad_center = [89.2469, 92.4957] # in mm
    cspad_radii = [25, 70]
    cspad_x, cspad_y = detectors['cspad'].coords_xy(run)

    cspad_x = (cspad_x/1000 - cspad_center[0]).ravel()
    cspad_y = (cspad_y/1000 - cspad_center[1]).ravel()

    cspad_r, cspad_theta = cart2polar(cspad_x, cspad_y)
    rbinned = np.digitize(cspad_r, [0., cspad_radii[0], cspad_radii[1], 1000])
    cspad_shape = detectors['cspad'].shape()
    cspad_det = detectors['cspad']
    cspad_mask = np.zeros(cspad_shape, dtype=np.int8).ravel()
    cspad_mask[rbinned == 2] = 1
    cspad_mask = cspad_mask.reshape(cspad_shape)
    
    # list epics vars
    epics_vars = [
        'lxe',
        'lxt',
        'lxt_ttc',
        #'XPP-SB3-BMMON',
        'las_lensv',
        'las_lensh',
        'las_lensf',
        'las_tt_wp',
        'las_opa_wp',
        'ccm_E',
        'las_delayNew',
        'XPP:TIMETOOL:AMPL',
        'XPP:TIMETOOL:AMPLNXT',
        'XPP:TIMETOOL:FLTPOS',
        'XPP:TIMETOOL:FLTPOSFWHM',
        'XPP:TIMETOOL:FLTPOS_PS',
        'XPP:TIMETOOL:REFAMPL',
        ]
    epics_dtype = np.dtype([(name.replace(':','_'), np.float64) for name in epics_vars])

    def _process_evr(evt):
        # reads control data, and evr0
        evr = detectors['evr0']
        codes = X340EVR.NONE
        ec = evr(evt)
        if ec is None:
          ec = []
          codes = X340EVR.MISSING

        if 90 in ec:
            codes = codes | X340EVR.LASERON

        if 91 in ec:
            codes = codes | X340EVR.LASEROFF
        
        if 163 in ec:
            codes = codes | X340EVR.XFELOFF

        return int(codes),

    def _process_control(evt):
        # unfortunately, the ControlData pointer to the current event is passed in from the environment
        # not by passing in the event

        det = detectors['ControlData']
        known_controls = {'ccmE_vernier': None}
        cfg = det()

        for c in cfg.pvControls():
            if c.name() in known_controls:
                known_controls[c.name()] = c.value()

        return known_controls.get('ccmE_vernier', np.nan),

    def _process_ipm(evt):
        data = np.nan*np.ones(2)
        
        for i, name in enumerate(['XPP-SB2-BMMON', 'XPP-SB3-BMMON']):
            det = detectors[name]
            tmp = det.get(evt)
            value = None if tmp is None else tmp.TotalIntensity()
            data[i] = value

        return data
    
    def _process_epics(evt):
        data = np.nan*np.ones(len(epics_vars))
        for i, name in enumerate(epics_vars):
            try:
                det = psana.Detector(name)
                value = det(evt) # extract value
            except KeyError:
                value = None # detector missing

            data[i] = value
        return data

    def _process_cspad(evt):
        # function to process cspad data
        det = detectors['cspad']
        threshold = 20.
        mbits = MbitMask.PIXEL_MASK | MbitMask.PIXEL_STATUS | MbitMask.UNBONDED4 | MbitMask.CSPAD_CENTRAL | MbitMask.EDGE_PIXELS
        cmpars = None
        calib = det.calib(evt, mbits=mbits, cmpars=cmpars)

        if calib is None:
            isgood = IsGoodFlag.NONE
            data = 0.
        else:
            isgood = IsGoodFlag.GOOD
            data = custom_threshold(calib, lower=threshold, lvalue=0.)
            data = data*cspad_mask
            data = data.sum(dtype=np.float64)

        return data, isgood

    def _process_epix10k(evt):
        # function to process epix data
        det = detectors['epix_1']
        threshold = 20.
        mbits = MbitMask.PIXEL_MASK | MbitMask.PIXEL_STATUS
        cmpars = None
        calib = det.calib(evt, mbits=mbits, cmpars=cmpars)

        if calib is None:
            isgood = IsGoodFlag.NONE
            roi1_sum = 0.
            roi2_sum = 0.
        else:
            isgood = IsGoodFlag.GOOD
            data = custom_threshold(calib, lower=threshold, lvalue=0.)
            roi1 = data*epix_mask1 # enable masking
            roi2 = data*epix_mask2 # enable masking
            roi1_sum = roi1.sum(dtype=np.float64)
            roi2_sum = roi2.sum(dtype=np.float64)

        return roi1_sum, roi2_sum

    detector_map = {
        'cspad': DetCtx(callable=_process_cspad,
                            dtype=np.dtype([('sum', np.float64), ('isgood', np.int64)]),
                            mpi_dtype=ARRAYDET_DTYPE,
                            data=None),
        'epix_1': DetCtx(callable=_process_epix10k,
                            dtype=np.dtype([
                              ('filt_sum', np.float64), 
                              ('open_sum', np.float64)]),
                            mpi_dtype=EPIXDET_DTYPE,
                            data=None),
        'ControlData': DetCtx(callable=_process_control,
                         dtype=np.dtype([('ccmE_vernier', np.float64),]),
                         mpi_dtype=CONTROL_DTYPE,
                         data=None),
        'epics': DetCtx(callable=_process_epics,
                         dtype=epics_dtype,
                         mpi_dtype=EPICS_DTYPE,
                         data=None),
        'ipms': DetCtx(callable=_process_ipm,
                              dtype=np.dtype([('ipm2', np.float64),
                                              ('ipm3', np.float64)]),
                              mpi_dtype=IPMDET_DTYPE,
                              data=None),
        'evr0': DetCtx(callable=_process_evr,
                              dtype=np.dtype([('flags', np.int32)]),
                              mpi_dtype=MPI.INT32_T,
                              data=None),
    }

    return detector_map

def allocate_chunk_data(detector_map, chunk_size):
    detector_data = {}
    for name, ctx in detector_map.items():
        # creates a new context with allocated data
        detector_data[name] = DetCtx(callable=ctx.callable,
                                     dtype=ctx.dtype,
                                     mpi_dtype=ctx.mpi_dtype,
                                     data=np.empty(chunk_size, dtype=ctx.dtype))
    return detector_data

def make_EventTime(timestamps):
    for ts, fid in timestamps:
        yield psana.EventTime(ts, fid)

@click.command()
@click.option('--output-dir', default=output_dir, help="directory used when writing reduced data files")
@click.option('--output-template', default=output_template, help="naming scheme used for data files")
@click.option('--test/--no-test', default=False, help="analyze first 50 frames to verify that script runs")
@click.argument('run', type=int)
def process_data(run, output_dir, output_template, test):
    """analyze a run from experiment x340 given the run number.

    The output directory and template options will interpolate the strings {exp_name} with an experiment string like
    xppx34017 (set in file) and {run} with the run number passed on the command line
    """

    expstr = datasource_template.format(run=run, exp_name=exp_name)
    ds = psana.DataSource(expstr)
    _run = next(ds.runs())
    dettuple = psana.DetNames()
    detnames = [d[0] if d[1] == '' else d[1] for d in dettuple]
    event_times, count = None, None
    if rank == 0: # only print from main process
        event_times = _run.times()
        count = 500 if test else len(event_times)
        event_times = event_times[:count]

        print('Run {} has {} events'.format(expstr, count))
        print('Found {} detectors:'.format(len(detnames)))
        for name in detnames:
            print('\t' + name)

        print('Available ControlData is')
        for _c in psana.Detector('ControlData')().pvControls():
            print('\t' + _c.name())
    detector_map = configure_detectors(detnames, run)


    # split events into chunks for processing, then convert them to sizes and offsets for scatterv
    timestamps = None
    chunks_sizes = np.empty((size + 1,), dtype=np.int32)
    if rank == 0:
        # serialize events to be able to send them as numpy arrays
        timestamps = np.zeros((count,), dtype=timestamp_dtype)
        assert len(event_times) == count, "must have `count` event times"

        for i, et in enumerate(event_times):
            timestamps[i] = int(et.seconds() << 32 | et.nanoseconds()), et.fiducial()

        # chunk the timestamps based on rank to use scatterv
        max_chunk = count//size + 1
        chunks = list(toolz.partition_all(max_chunk, range(count)))
        chunks_sizes[1:] = map(len, chunks)
        chunks_sizes[0] = 0

    comm.Bcast(chunks_sizes) # broadcast chunk sizes to all workers
    chunks_offsets = np.cumsum(chunks_sizes, dtype=np.int64)[:-1]
    chunks_sizes = chunks_sizes[1:]

    # Send timestamps to workers
    sendbuf = [timestamps, chunks_sizes, chunks_offsets, TIMESTAMP_DTYPE]
    chunk = np.empty((chunks_sizes[rank]), dtype=timestamp_dtype)
    recvbuf = [chunk, chunks_sizes[rank], TIMESTAMP_DTYPE]

    if rank == 0:
        print('Rank {}, sizes {}'.format(rank, chunks_sizes))

    # send timestamps to all workers
    comm.Scatterv(sendbuf, recvbuf)

    # allocate data for each detector in array
    chunk_map = allocate_chunk_data(detector_map, chunks_sizes[rank])

    # begin processing results
    t0 = time.clock()
    for i, et in enumerate(make_EventTime(chunk)):
        if all([i % 200 == 0, i > 0, rank == 0]):
            print('Rank {rank}, processed {i} frames in {time}s'.format(rank=rank, i=i+1, time=time.clock() - t0))

        # get the event at EventTime `et`
        evt = _run.event(et)

        for name, ctx in chunk_map.items(): # process each detector for this event
            ctx.data[i] = ctx.callable(evt)

    # gather events and write them to disk in main process
    if rank == 0:
        # allocate space for each detector
        aggregate_map = allocate_chunk_data(chunk_map, count)
    else:
        aggregate_map = allocate_chunk_data(chunk_map, 0)

    t1 = time.clock()
    for name, ctx in aggregate_map.items():
        chunkctx = chunk_map[name]
        sendbuf = [chunkctx.data, chunks_sizes[rank], chunkctx.mpi_dtype]
        recvbuf = [ctx.data, chunks_sizes, chunks_offsets, chunkctx.mpi_dtype]
        comm.Gatherv(sendbuf, recvbuf)
    t2 = time.clock()
    
    if rank == 0:
      print('Rank {rank}, gathered {count} frames in {time}s'.format(rank=rank,
        count=count, time=t2-t1))

    t3 = time.clock()
    # write detector output to file in main process
    if rank == 0:
        fp = os.path.join(output_dir, output_template.format(run=run, exp_name=exp_name))
        if not os.path.exists(output_dir):
            print("Directory {} doesn't exist; aborting".format(output_dir))
            return -1

        with h5py.File(fp, 'w') as f:
            for name, ctx in aggregate_map.items():
                name = name.replace(':', '_')
                f.create_dataset(name, (count,), data=ctx.data)

            f.create_dataset('timestamps', data=timestamps)

        print('Wrote {} detectors to file {} in {time}s'.format(len(aggregate_map), fp, time=time.clock()-t3))
        return 0

if __name__ == '__main__':
    process_data()
